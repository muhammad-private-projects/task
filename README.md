Docker Layer Maker
==================

Some Docker registries have limits on the size that an individual layer can be. This can pose a problem for some very large Docker images. A MATLAB installation for example might be 10s of GiB if a lot of toolboxes are installed.

One way to workaround this issue is to create a new Docker image which does not copy more than a certain amount of data into each layer. Each part subdirectory is a different size and thus a different number of them can be incorporated into each layer.

```Dockerfile
FROM ubuntu
COPY data/part1 data/part2 data/part3 /data
COPY data/part4 /data
COPY data/part5 data/part6 data/part7 /data
```

In order to make such a Dockerfile, we need to know which parts can be placed into each layer.

The objective of this assignment is to create a script that will partition a directory of files so that each layer is close to, but not exceeding a certain limit. The script should first determine which parts can go in each layer, then should write out a Dockerfile which can then be built.

Accompanying this README is a python script, create-data.py that will write a directory called data in the current working directory.

```bash
cd docker-layer-maker
python create-data.py
ls -alh data/
```

As you can see, a hierarchy of binary files have been written into the `data` directory that are between 1 (although depending on your filesystem's blocksize the minimum size might be greater) and 512 KiB in size. For this exercise, we will impose a layer limit (for the layers we are adding with data) of 5 MiB.

Task
----

1. Write a Python script that partitions the files ready for `COPY` within each layer.
2. Make that script output a `Dockerfile` that has the `COPY` commands as calculated by step (1).
3. Build the Docker image from the Dockerfile and confirm through automated testing that the added layers are below the required 5 MiB threshold.

Notes
-----
* Each top-level directory within `data` is guaranteed to be less than the 5 MiB layer limit.
* You can assume that each directory which is a direct subdirectory of `data` is atomic. I.e. That there is no need to further partition below that level. I.e. a layer might `COPY` into the Docker image `data/dir1, data/dir2, data/dir3`. There is no need to partition such that you would `COPY` `data/dir1/subdir1, data/dir1/subdir2`, etc.
* The Docker layers will probably not match the calculated size of the partition exactly, this is fine. You could allow for some amount of "overhead" in your solution though so that the layers are definitely under 5 MiB.