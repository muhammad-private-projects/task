import sys
import os
import random
from pathlib import Path
from uuid import uuid4

# Create 100 MiB (or slightly more) total data in all files
create_data = 100 * 1024 * 1024

# 3 MiB Maximum data in one top-level directory
max_dir_data = 3 * 1024 * 1024

# 512 KiB Maximum data in one file
max_file_size = 512 * 1024

# Maximum files per directory
max_files_per_dir = 5

# Maximum directory nesting depth
max_dir_depth = 5

# Percentage chance to create sub-dir
dir_chance = 75


def create_files_and_dirs(
    parent_path,
    current_max_dir_data,
    current_dir_depth=0,
    current_dir_data=0
):
    # create some number of files
    num_files = random.randint(0, max_files_per_dir)
    for i in range(num_files):
        if current_dir_data > current_max_dir_data:
            return current_dir_data

        file_size = random.randint(1, max_file_size)
        with open(parent_path / f'part-{uuid4()}', 'wb') as f:
            f.write(os.urandom(file_size))
        current_dir_data += file_size

    # Possibly create sub-directory unless max depth reached
    if current_dir_depth < max_dir_depth:
        if random.randint(1, 100) <= dir_chance:
            child_path = parent_path / f'dir-{uuid4()}'
            child_path.mkdir()

            # Recurse
            current_dir_data = create_files_and_dirs(
                child_path,
                current_max_dir_data,
                current_dir_depth + 1,
                current_dir_data
            )

    return current_dir_data


data_dir = Path() / 'data'

if data_dir.exists():
    print('data directory exists, delete before re-running this script')
    sys.exit(1)

os.mkdir(data_dir)
random.seed(0)

# Create directories
total_data_created = 0
while total_data_created <= create_data:
    total_data_created += create_files_and_dirs(
        data_dir,
        random.randint(max_file_size, max_dir_data)
    )
