import os
import shutil
import time
import docker
import uuid
from ortools.linear_solver import pywraplp
from tqdm import tqdm


# papers / resources
# https://github.com/dayuebai/Bin-Packing
# https://www.inf.ufpr.br/aurora/disciplinas/topicosia2/problemas/BinPackingHyFlex.pdf
# https://www.sciencedirect.com/science/article/abs/pii/S0305054800000824

MEGABYTE = 1048576  # 1 MiB in bytes
THRESHOLD = 5 * MEGABYTE  # 5 MiB in bytes
DOCKERDIR = "data"  # Docker data directory
DOCKERFILE = "Dockerfile"  # Output Dockerfile
OVERHEAD = 1 * MEGABYTE  # overhead
BASE_IMAGE = "ubuntu"  # Base image to use


def get_directories(path):
    """Return all directories inside the specified path."""
    return [entry.path for entry in os.scandir(path) if entry.is_dir()]


def get_directory_size(path):
    """Return total size (in bytes) of all files inside the specified path."""
    return sum(
        entry.stat().st_size for entry in os.scandir(path) if entry.is_file())


def first_fit_bin_pack(directories, max_size, overhead):
    """Partition directories into bins using 'first fit' algorithm."""
    directories = sorted(directories, key=lambda x: x[1], reverse=True)
    groups = []

    for dir_path, dir_size in directories:
        adjusted_dir_size = dir_size + overhead
        for group in groups:
            if sum(size for path, size in group) + adjusted_dir_size <= max_size:
                group.append((dir_path, dir_size))  # store original size
                break
        else:
            groups.append([(dir_path, dir_size)])

    return groups


def ortools_bin_pack(directories, max_size, overhead):
    """Uses SCIP backend to solve bin packing problem."""

    data = {"weights": [size for path, size in directories],
            "items": list(range(len(directories)))}

    data["bins"] = data["items"]
    data["bin_capacity"] = max_size - overhead

    solver = pywraplp.Solver.CreateSolver("SCIP")
    if not solver:
        return

    # Variables
    # x[i, j] = 1 if item i is packed in bin j.
    x = {}
    for i in data["items"]:
        for j in data["bins"]:
            x[i, j] = solver.IntVar(0, 1, f"x_{i}_{j}")

    # y[j] = 1 if bin j is used.
    y = {}
    for j in data["bins"]:
        y[j] = solver.IntVar(0, 1, f"y_{j}")

    # Constraints
    for i in data["items"]:
        solver.Add(sum(x[i, j] for j in data["bins"]) == 1)

    for j in data["bins"]:
        solver.Add(
            sum(x[i, j] * data["weights"][i] for i in data["items"])
            <= y[j] * data["bin_capacity"]
        )

    solver.Minimize(solver.Sum([y[j] for j in data["bins"]]))

    solver.Solve()

    groups = []
    for j in data["bins"]:
        if y[j].solution_value() == 1:
            group = []
            for i in data["items"]:
                if x[i, j].solution_value() > 0:
                    group.append(directories[i])
            if group:
                groups.append(group)
    return groups


def best_fit_bin_pack(directories, max_size, overhead):
    """Partition directories into groups/bins using 'best fit' algorithm.
    Each group's total size does not exceed the maximum size limit"""
    # sort directories by size in decreasing order
    directories = sorted(directories, key=lambda x: x[1], reverse=True)
    groups = []

    for dir_path, dir_size in directories:
        best_space_left = max_size + 1
        best_index = -1
        adjusted_dir_size = dir_size + overhead
        for i in range(len(groups)):
            if adjusted_dir_size <= max_size - \
                    sum(size for path, size in groups[i]) < best_space_left:
                best_space_left = max_size - sum(size for path, size in groups[i])
                best_index = i

        if best_index == -1:
            groups.append([(dir_path, dir_size)])
        else:
            groups[best_index].append((dir_path, dir_size))

    return groups


def generate_dockerfile(file_name, groups):
    """Generate Dockerfile with COPY commands for each group."""
    with open(file_name, "w") as f:
        f.write("FROM ubuntu\n")
        for group in groups:
            paths = " ".join(path for path, size in group)
            f.write(f"COPY {paths} /{DOCKERDIR}\n")


def verify_file_size(image_name, base_image_name, threshold=THRESHOLD):
    client = docker.from_env()
    image = client.images.get(image_name)
    base_image = client.images.get(base_image_name)

    # Get the layer count of the base image
    base_layers_count = len(base_image.history())

    # calculate percentage of layers under threshold
    under_threshold = 0
    for layer in image.history()[:-base_layers_count]:
        if layer["Size"] < threshold:
            under_threshold += 1

    divisor = len(image.history()[:-base_layers_count])
    if divisor == 0:
        return 0
    return under_threshold / divisor


def create_dockerfile(method, threshold=THRESHOLD, overhead=OVERHEAD,
                      cleanup_images=True):
    dir_sizes = [(dir_path, get_directory_size(dir_path)) for dir_path in
                 get_directories(DOCKERDIR)]

    # Partition directories into bins and Generate Dockerfile
    groups = method(dir_sizes, threshold, overhead)
    run_id = str(uuid.uuid4()).replace("-", "")

    file_name = f"Dockerfile_{run_id}"
    image_name = f"docker_image_{run_id}"

    generate_dockerfile(file_name, groups)

    # Measure the time taken to build the Docker image
    start_time = time.time()

    # Run shell command to build docker image, run silently
    os.system(f"docker build -t {image_name} -f {file_name} . > /dev/null 2>&1")

    end_time = time.time()
    elapsed_time = end_time - start_time

    # percentage of bins under threshold
    score = verify_file_size(image_name, BASE_IMAGE, threshold)

    # delete created docker image
    if cleanup_images:
        os.system(f"docker rmi {image_name} > /dev/null 2>&1")

    return run_id, score, elapsed_time


def main():
    """ 1. delete /data directory if it exists
        2. run create-data.py
        3. run against all three bin packing algorithms
        4. repeat the steps 1-3 10 times
        5. print an array containing the results of each runs, and the average
        runtime and scores for each algorithm"""


    # run tests
    results = []
    for i in tqdm(range(10)):
        if os.path.exists(DOCKERDIR):
            shutil.rmtree(DOCKERDIR)
        os.system("python create-data.py")
        results.append(create_dockerfile(first_fit_bin_pack))
        results.append(create_dockerfile(ortools_bin_pack))
        results.append(create_dockerfile(best_fit_bin_pack))

    # delete data
    shutil.rmtree(DOCKERDIR)

    # print results
    for i in range(0, len(results), 3):
        print("Run ID: ", results[i][0])
        print("First Fit Score: ", results[i][1])
        print("First Fit Time: ", results[i][2])
        print("OR Tools Score: ", results[i + 1][1])
        print("OR Tools Time: ", results[i + 1][2])
        print("Best Fit Score: ", results[i + 2][1])
        print("Best Fit Time: ", results[i + 2][2])
        print()

    # print average
    first_fit_scores = [results[i][1] for i in range(0, len(results), 3)]
    first_fit_time = [results[i][2] for i in range(0, len(results), 3)]
    ortools_scores = [results[i][1] for i in range(1, len(results), 3)]
    ortools_time = [results[i][2] for i in range(1, len(results), 3)]
    best_fit_scores = [results[i][1] for i in range(2, len(results), 3)]
    best_fit_time = [results[i][2] for i in range(2, len(results), 3)]

    print("------------Average Scores and Times------------")
    print("First Fit Average Score: ", sum(first_fit_scores) / len(first_fit_scores))
    print("First Fit Average Time: ", sum(first_fit_time) / len(first_fit_time))
    print("OR Tools Average Score: ", sum(ortools_scores) / len(ortools_scores))
    print("OR Tools Average Time: ", sum(ortools_time) / len(ortools_time))
    print("Best Fit Average Score: ", sum(best_fit_scores) / len(best_fit_scores))
    print("Best Fit Average Time: ", sum(best_fit_time) / len(best_fit_time))


if __name__ == '__main__':
    main()
